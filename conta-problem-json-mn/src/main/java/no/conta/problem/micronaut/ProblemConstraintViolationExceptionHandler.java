/*
 * Copyright (c) 2021-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import io.micronaut.http.server.exceptions.response.ErrorContext;
import io.micronaut.http.server.exceptions.response.ErrorResponseProcessor;
import io.micronaut.validation.exceptions.ConstraintExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

import no.conta.problem.Problem;
import no.conta.problem.StandardProblemCode;
import no.conta.problem.Violation;

/**
 * Modified copy of {@link io.micronaut.problem.violations.ProblemConstraintViolationExceptionHandler}
 */
@Produces
@Singleton
@Requires(classes = {ConstraintViolationException.class, ExceptionHandler.class})
@Replaces(ConstraintExceptionHandler.class)
public class ProblemConstraintViolationExceptionHandler implements ExceptionHandler<ConstraintViolationException, HttpResponse<?>> {

    private static final Logger log = LoggerFactory.getLogger(ProblemConstraintViolationExceptionHandler.class);

    private final ErrorResponseProcessor<?> responseProcessor;

    @Inject
    public ProblemConstraintViolationExceptionHandler(ErrorResponseProcessor<?> responseProcessor) {
        this.responseProcessor = responseProcessor;
    }

    @Override
    public HttpResponse<?> handle(HttpRequest request, ConstraintViolationException exception) {
        log.debug("Handle constraint problem: [{}, {}]", exception.getClass(), exception.getMessage());
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        return responseProcessor.processResponse(
            ErrorContext.builder(request)
                .cause(
                    Problem.of(httpStatus.getCode(), StandardProblemCode.ConstraintViolation)
                        .withInfo("violations", mapToViolationsList(exception))
                        .toThrowable()
                )
                .errorMessage(exception.getMessage())
                .build(),
            HttpResponse.status(httpStatus)
        );
    }

    @NonNull
    private List<Violation> mapToViolationsList(ConstraintViolationException exception) {
        return exception.getConstraintViolations().stream()
            .map(this::createViolation)
            .collect(Collectors.toList());
    }

    @NonNull
    private Violation createViolation(@NonNull ConstraintViolation<?> constraintViolation) {
        return Violation.of(
            constraintViolation.getPropertyPath().toString(),
            constraintViolation.getMessage()
        );
    }

}
