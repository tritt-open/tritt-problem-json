/*
 * Copyright (c) 2021-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import io.micronaut.http.server.exceptions.response.ErrorContext;
import io.micronaut.http.server.exceptions.response.ErrorResponseProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Singleton;

import no.conta.problem.ThrowableProblem;

/**
 * Modified copy of {@link io.micronaut.problem.ThrowableProblemHandler}
 */
@Produces
@Singleton
public class ThrowableProblemHandler implements ExceptionHandler<ThrowableProblem, HttpResponse<?>> {

    private static final Logger log = LoggerFactory.getLogger(ThrowableProblemHandler.class);

    private final ErrorResponseProcessor<?> responseProcessor;

    public ThrowableProblemHandler(ErrorResponseProcessor<?> responseProcessor) {
        this.responseProcessor = responseProcessor;
    }

    @Override
    public HttpResponse<?> handle(HttpRequest request, ThrowableProblem exception) {
        if (log.isTraceEnabled()) {
            log.trace("Handle throwable problem [{}, {}].", exception.getDetail(), exception.getCode(), exception);
        } else if (log.isDebugEnabled()) {
            log.debug("Handle throwable problem [{}, {}]", exception.getDetail(), exception.getCode());
        }
        return responseProcessor.processResponse(
            ErrorContext.builder(request)
                .cause(exception)
                .errorMessage(exception.getMessage())
                .build(),
            HttpResponse.status(HttpStatus.valueOf(exception.getStatus()))
        );
    }

}
