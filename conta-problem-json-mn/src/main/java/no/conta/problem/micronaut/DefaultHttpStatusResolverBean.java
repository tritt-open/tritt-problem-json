/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.http.HttpStatus;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import no.conta.problem.ThrowableProblem;

/**
 * Default implementation of {@link HttpStatusResolver}. It defaults to {@link ProblemJsonConfigurationProperties#defaultHttpStatus()} when
 * the status is not defined for the {@link ThrowableProblem}.
 *
 * @see ThrowableProblem
 * @see HttpStatusResolver
 * @see ProblemJsonConfigurationProperties
 */
@Singleton
public class DefaultHttpStatusResolverBean implements HttpStatusResolver {

    private final ProblemJsonConfiguration config;

    @Inject
    public DefaultHttpStatusResolverBean(ProblemJsonConfiguration config) {
        this.config = config;
    }

    /**
     * Default implementation of {@link HttpStatusResolver}. It defaults to {@link ProblemJsonConfigurationProperties#defaultHttpStatus()} when
     * the status is not defined for the {@link ThrowableProblem}.
     */
    @Override
    public HttpStatus resolveStatus(@NonNull ThrowableProblem problem) {

        if (problem.getStatus() != null) {
            return HttpStatus.valueOf(problem.getStatus());
        }

        return config.defaultHttpStatus();
    }
}
