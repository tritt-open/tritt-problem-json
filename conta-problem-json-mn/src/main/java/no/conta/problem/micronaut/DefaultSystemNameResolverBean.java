/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.runtime.ApplicationConfiguration;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import no.conta.problem.ThrowableProblem;

/**
 * Default implementation of {@link SystemNameResolver}.
 * It defaults to {@link ProblemJsonConfigurationProperties#getDefaultSystemName()} when the status is not defined for the {@link ThrowableProblem}.
 * If {@link ProblemJsonConfigurationProperties#getDefaultSystemName()} is not set then it falls back to {@link ApplicationConfiguration#getName()}.
 *
 * @see ThrowableProblem
 * @see ProblemJsonConfigurationProperties
 * @see ApplicationConfiguration
 */
@Singleton
public class DefaultSystemNameResolverBean implements SystemNameResolver {

    private final ProblemJsonConfiguration problemJsonConfiguration;
    private final ApplicationConfiguration applicationConfiguration;

    @Inject
    public DefaultSystemNameResolverBean(
        ProblemJsonConfiguration problemJsonConfiguration,
        ApplicationConfiguration applicationConfiguration
    ) {
        this.problemJsonConfiguration = problemJsonConfiguration;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    public String resolveSystemName(@NonNull ThrowableProblem problem) {

        if (problem.getSystemName() != null) {

            return problem.getSystemName();
        }

        return resolveDefaultSystemName();
    }

    @Override
    public String resolveDefaultSystemName() {

        if (problemJsonConfiguration.getDefaultSystemName() != null) {
            return problemJsonConfiguration.getDefaultSystemName();
        }

        return applicationConfiguration.getName().orElse(null);
    }
}
