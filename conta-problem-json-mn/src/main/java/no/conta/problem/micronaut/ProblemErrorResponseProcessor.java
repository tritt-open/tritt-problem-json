/*
 * Copyright (c) 2021-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.server.exceptions.response.Error;
import io.micronaut.http.server.exceptions.response.ErrorContext;
import io.micronaut.http.server.exceptions.response.ErrorResponseProcessor;
import io.opentracing.SpanContext;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import no.conta.problem.Problem;
import no.conta.problem.Problem.ProblemBuilder;
import no.conta.problem.Problem.Trace;
import no.conta.problem.ThrowableProblem;

/**
 * Modified copy of {@link io.micronaut.problem.ProblemErrorResponseProcessor}
 */
@Singleton
public class ProblemErrorResponseProcessor implements ErrorResponseProcessor<Problem> {

    public static final String APPLICATION_PROBLEM_JSON = "application/problem+json";

    private static final Logger log = LoggerFactory.getLogger(ProblemErrorResponseProcessor.class);

    private final HttpStatusResolver httpStatusResolver;
    private final SystemNameResolver systemNameResolver;
    private final Tracer tracer;

    @Inject
    public ProblemErrorResponseProcessor(HttpStatusResolver httpStatusResolver, SystemNameResolver systemNameResolver, @Nullable Tracer tracer) {
        this.httpStatusResolver = httpStatusResolver;
        this.systemNameResolver = systemNameResolver;
        this.tracer = tracer;
    }

    @Override
    @NonNull
    public MutableHttpResponse<Problem> processResponse(@NonNull ErrorContext errorContext, @NonNull MutableHttpResponse<?> baseResponse) {
        return baseResponse
            .contentType(APPLICATION_PROBLEM_JSON)
            .body(mapToProblem(errorContext, baseResponse.getStatus()));
    }

    @NonNull
    protected Problem mapToProblem(@NonNull ErrorContext errorContext, @NonNull HttpStatus httpStatus) {
        return errorContext.getRootCause()
            .map(cause -> constructFromCause(errorContext, httpStatus, cause))
            .orElseGet(() -> constructFromHttpStatus(errorContext, httpStatus))
            .withTrace(extractTrace());
    }

    @NonNull
    protected Problem constructFromCause(@NonNull ErrorContext errorContext, @NonNull HttpStatus httpStatus, Throwable cause) {
        if (cause instanceof ThrowableProblem tp) {
            log.debug("Map throwable problem to problem");

            HttpStatus status = httpStatusResolver.resolveStatus(tp);
            String systemName = systemNameResolver.resolveSystemName(tp);

            return Problem.builder()
                .title(status.getReason())
                .status(status.getCode())
                .systemName(systemName)
                .detail(tp.getDetail())
                .code(tp.getCode())
                .info(tp.getInfo())
                .build();
        }

        if (log.isDebugEnabled()) {
            log.debug("Map error to problem: [{}, {}]", cause.getClass(), cause.getMessage(), cause);
        }
        return mapErrorToProblem(errorContext, httpStatus);
    }

    @NonNull
    protected Problem constructFromHttpStatus(@NonNull ErrorContext errorContext, @NonNull HttpStatus httpStatus) {
        log.debug("No root cause, create simple problem. Errors: {}", errorContext.getErrors());
        return Problem.of(httpStatus.getCode(), httpStatus.getReason());
    }

    private Trace extractTrace() {
        if (tracer == null || tracer.activeSpan() == null) {
            return null;
        }
        SpanContext ctx = tracer.activeSpan().context();
        return Trace.of(ctx.toTraceId(), ctx.toSpanId());
    }

    @NonNull
    private Problem mapErrorToProblem(@NonNull ErrorContext errorContext, @NonNull HttpStatus httpStatus) {
        ProblemBuilder builder = Problem.builder()
            .title(httpStatus.getReason())
            .status(httpStatus.getCode())
            .systemName(systemNameResolver.resolveDefaultSystemName());

        if (errorContext.hasErrors()) {
            if (log.isDebugEnabled()) {
                log.debug("errors: {}", concatErrors(errorContext.getErrors()));
            }

            Error error = errorContext.getErrors().get(0);
            builder.detail(error.getMessage());
            error.getTitle().ifPresent(
                title -> builder.title(title)
            );
            error.getPath().ifPresent(
                path -> builder.info(Map.of("message", error.getMessage(), "path", path))
            );
        }

        return builder.build();
    }

    @NonNull
    private static String concatErrors(List<Error> errors) {
        return errors.stream()
            .map(error -> error.getMessage())
            .collect(Collectors.joining(", "));
    }

}
