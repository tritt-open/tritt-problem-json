package no.conta.problem

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import io.micronaut.jackson.modules.BeanIntrospectionModule
import spock.lang.Shared
import spock.lang.Specification

class RemarkTest extends Specification {

    @Shared private ObjectMapper objectMapper = JsonMapper.builder()
        .addModule(new BeanIntrospectionModule())
        .build()

    def 'remark with actions'() {
        when:
            def remark = Remark.of('', RemarkType.Warning, 666)
                .withAction(RemarkAction.of('action-key'))
        then:
            remark
            remark.actions.size() == 1

        when:
            def changed = remark.withAction(RemarkAction.of('action-key-2'))
        then:
            changed.actions.size() == 2
    }

    def 'remark <-> json'() {
        given:
            Remark r = Remark.of('fieldName', RemarkType.Note, 1337)
                .withInfo('key1', 'value1')
                .withAction(RemarkAction.of('do-stuff', [:]))
        when:
            String json = objectMapper.writeValueAsString(r)
        then:
            json
        when:
            Remark parsed = objectMapper.readValue(json, Remark)
        then:
            parsed.field == 'fieldName'
            parsed.actions.size() == 1
            parsed.actions[0].key == 'do-stuff'
        and:
            parsed == r
    }

    def 'scope remark to parent field'() {
        given:
            Remark initial = Remark.of('child', RemarkType.Note, 1448)
        when:
            Remark scoped = initial.scopedTo('parent')
        then:
            scoped.field == 'parent.child'
            scoped.type == initial.type
            scoped.code == initial.code
    }

    def 'change remark type'() {
        given:
            Remark initial = Remark.of('fieldName', RemarkType.Caution, 1448)
        when:
            Remark changed = initial.withChangedType(RemarkType.Important)
        then:
            changed != initial
            changed.field == initial.field
            changed.type == RemarkType.Important
            changed.code == initial.code
            changed.messageKey == initial.messageKey
            changed.info == initial.info
            changed.actions == initial.actions
    }
}
