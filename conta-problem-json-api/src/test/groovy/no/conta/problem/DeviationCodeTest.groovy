/*
 * Copyright (c) 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem

import spock.lang.Specification

class DeviationCodeTest extends Specification {

    @SuppressWarnings('GroovyAccessibility')
    def setupSpec() {
        CodeRegistry.@duplicatedCodes.clear()
    }

    def 'map from code to remark code'() {
        when:
            DeviationCode rc = DeviationCode.fromValue(value, expected.getClass())
        then:
            rc == expected
        where:
            value || expected
            6101  || CustomDeviationCode.DC1
            7102  || CustomDeviationCode.DC2
    }

    def 'discover conflicts'() {
        expect:
            CustomProblemCode.PC1.code == CustomDeviationCode.DC1.code
        and:
            !CodeAware.getDuplicatedCodes().empty
    }

    enum CustomProblemCode implements ProblemCode {
        PC1(6101),
        PC2(6102),
        PC3(6103)

        static {
            values().each { ProblemCode.add(it) }
        }

        int code
        Integer status = null

        CustomProblemCode(int code) {
            this.code = code
        }
    }

    enum CustomDeviationCode implements DeviationCode {
        DC1(6101),
        DC2(7102),

        static {
            values().each { DeviationCode.add(it) }
        }
        int code

        CustomDeviationCode(int code) {
            this.code = code
        }
    }

}
