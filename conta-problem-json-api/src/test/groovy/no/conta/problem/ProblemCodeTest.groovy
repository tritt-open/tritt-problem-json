/*
 * Copyright (c) 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem

import spock.lang.Specification

class ProblemCodeTest extends Specification {

    @SuppressWarnings('GroovyAccessibility')
    def setupSpec() {
        CodeRegistry.@duplicatedCodes.clear()
    }

    def 'map from code to standard problem code'() {
        when:
            ProblemCode pc = ProblemCode.fromValue(value, expected.getClass())
        then:
            pc == expected
        where:
            value || expected
            1001  || StandardProblemCode.ConstraintViolation
    }

    def 'discover conflicts'() {
        expect:
            DuplicatedProblemCode.Code1.code == DuplicatedProblemCode.Code2.code
        and:
            !CodeAware.getDuplicatedCodes().empty
    }

    enum DuplicatedProblemCode implements ProblemCode {
        Code1(5500),
        Code2(5500),
        Code3(5501)

        static {
            values().each { ProblemCode.add(it) }
        }

        int code
        Integer status = null

        DuplicatedProblemCode(int code) {
            this.code = code
        }
    }

}
