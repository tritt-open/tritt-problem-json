/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem

import spock.lang.Specification

class ThrowableProblemTest extends Specification {

    def 'building throwable problem without message or cause'() {
        when:
            ThrowableProblem tp = new ThrowableProblem(
                StandardProblemCode.ConstraintViolation.getStatus(),
                StandardProblemCode.ConstraintViolation.detail,
                StandardProblemCode.ConstraintViolation.code,
                null,
                ['validations': ['s is not a number', 'b cannot be null']]
            )
        then: 'the default status 400 is used'
            tp.status == 400
            tp.code == StandardProblemCode.ConstraintViolation.code
            tp.detail == StandardProblemCode.ConstraintViolation.detail
            tp.info.validations == ['s is not a number', 'b cannot be null']
            tp.message == null
            tp.cause == null
    }

    def 'building throwable problem with message'() {
        when:
            ThrowableProblem tp = new ThrowableProblem(
                StandardProblemCode.ConstraintViolation.getStatus(),
                StandardProblemCode.ConstraintViolation.detail,
                StandardProblemCode.ConstraintViolation.code,
                null,
                ['validations': ['s is not a number', 'b cannot be null']],
                'Validation Error'
            )
        then: 'the default status 400 is used'
            tp.status == 400
            tp.code == StandardProblemCode.ConstraintViolation.code
            tp.detail == StandardProblemCode.ConstraintViolation.detail
            tp.info.validations == ['s is not a number', 'b cannot be null']
            tp.message == 'Validation Error'
            tp.cause == null
    }

    def 'building throwable problem with message and cause'() {
        when:
            ThrowableProblem tp = new ThrowableProblem(
                StandardProblemCode.ConstraintViolation.getStatus(),
                StandardProblemCode.ConstraintViolation.detail,
                StandardProblemCode.ConstraintViolation.code,
                null,
                ['validations': ['s is not a number', 'b cannot be null']],
                'Validation Error',
                new RuntimeException('Some Validation Error')
            )
        then: 'the default status 400 is used'
            tp.status == 400
            tp.code == StandardProblemCode.ConstraintViolation.code
            tp.detail == StandardProblemCode.ConstraintViolation.detail
            tp.info.validations == ['s is not a number', 'b cannot be null']
            tp.message == 'Validation Error'
            tp.cause.message == 'Some Validation Error'
    }
}
