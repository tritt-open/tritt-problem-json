package no.conta.problem

import spock.lang.Specification

class InfoTest extends Specification {

    def 'build info map'() {
        when:
            def info = Info.of(
                'key1', 'value',
                'key2', null
            )
        then:
            info.key1 == 'value'
            info.key2 == null
            info.containsKey('key2')
    }

    def 'build info map without null values'() {
        when:
            def info = Info.ofNonNull(
                'key1', 'value',
                'key2', null
            )
        then:
            info.key1 == 'value'
            info.key2 == null
            !info.containsKey('key2')
    }
}
