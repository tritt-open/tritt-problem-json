package no.conta.problem

import spock.lang.Specification
import spock.lang.Unroll

class RemarkTypeTest extends Specification {

    def 'no overlap between ok and error types'() {
        expect:
            RemarkType.OK_TYPES.every { !RemarkType.ERROR_TYPES.contains(it) }
        and:
            RemarkType.ERROR_TYPES.every { !RemarkType.OK_TYPES.contains(it) }
        and:
            RemarkType.OK_TYPES.size() + RemarkType.ERROR_TYPES.size() == RemarkType.values().size()
    }

    @Unroll
    def 'remark type #type is#not ok type'() {
        expect:
            RemarkType.OK_TYPES.contains(type) == ok
        where:
            type                 | ok
            RemarkType.Ok        | true
            RemarkType.Note      | true
            RemarkType.Tip       | true
            RemarkType.Important | true
            RemarkType.Caution   | false
            RemarkType.Warning   | false
            RemarkType.Error     | false

            not = ok ? '' : ' not'
    }

    @Unroll
    def 'remark type #type is#not error type'() {
        expect:
            RemarkType.ERROR_TYPES.contains(type) == error
        where:
            type                 | error
            RemarkType.Ok        | false
            RemarkType.Note      | false
            RemarkType.Tip       | false
            RemarkType.Important | false
            RemarkType.Caution   | true
            RemarkType.Warning   | true
            RemarkType.Error     | true

            not = error ? '' : ' not'
    }

    @Unroll
    def 'increase #type -> #increased'() {
        expect:
            type.increase() == increased
        where:
            type                 | increased
            RemarkType.Ok        | type
            RemarkType.Note      | type
            RemarkType.Tip       | type
            RemarkType.Important | RemarkType.Caution
            RemarkType.Caution   | RemarkType.Warning
            RemarkType.Warning   | RemarkType.Error
            RemarkType.Error     | type
    }

    @Unroll
    def 'decrease #type -> #decreased'() {
        expect:
            type.decrease() == decreased
        where:
            type                 | decreased
            RemarkType.Ok        | type
            RemarkType.Note      | type
            RemarkType.Tip       | type
            RemarkType.Important | type
            RemarkType.Caution   | RemarkType.Important
            RemarkType.Warning   | RemarkType.Caution
            RemarkType.Error     | RemarkType.Warning
    }

    @Unroll
    def 'find most severe from remarks #remarks -> #mostSevere'() {
        expect:
            RemarkType.mostSevereFrom(remarks) == mostSevere
        where:
            types                                            | mostSevere
            null                                             | null
            []                                               | null
            [RemarkType.Important]                           | RemarkType.Important
            [RemarkType.Error, RemarkType.Important]         | RemarkType.Error
            [RemarkType.Important, RemarkType.Error]         | RemarkType.Error
            [RemarkType.Important, null, RemarkType.Caution] | RemarkType.Caution

            remarks = types?.collect { RemarkType type -> Remark.of('', type, 31) }
    }

}
