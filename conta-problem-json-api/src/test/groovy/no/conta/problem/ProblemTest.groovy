/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem

import spock.lang.Specification
import spock.lang.Unroll

class ProblemTest extends Specification {

    def 'building problem'() {
        when:
            def p = Problem.builder()
                .status(400)
                .title('Bad Request')
                .code(StandardProblemCode.ConstraintViolation.code)
                .detail(StandardProblemCode.ConstraintViolation.detail)
                .info('validations': ['s is not a number', 'b cannot be null'])
                .build()
                .with('extra', 'extra information')
                .withTrace(Problem.Trace.of('12345', '1234567890'))
        then:
            p.status == 400
            p.title == 'Bad Request'
            p.code == StandardProblemCode.ConstraintViolation.code
            p.detail == StandardProblemCode.ConstraintViolation.detail
            p.info.size() == 2
            p.info.validations == ['s is not a number', 'b cannot be null']
            p.info.extra == 'extra information'
            p.trace.traceId == '12345'
            p.trace.spanId == '1234567890'
    }

    def 'building problem using default status'() {
        when:
            Problem p = Problem.of(StandardProblemCode.ConstraintViolation)
                .withInfo('validations', ['s is not a number', 'b cannot be null'])
        then: 'the default status 400 is used'
            p.status == 400
            p.code == StandardProblemCode.ConstraintViolation.code
            p.detail == StandardProblemCode.ConstraintViolation.detail
            p.info.validations == ['s is not a number', 'b cannot be null']
    }

    @Unroll
    def "build problem using provided status #providedStatus"() {
        when: 'using given status'
            Problem p = Problem.of(
                providedStatus,
                StandardProblemCode.ConstraintViolation
            )
        then: 'the provided status is used'
            p.status == providedStatus
            p.code == StandardProblemCode.ConstraintViolation.code
            p.detail == StandardProblemCode.ConstraintViolation.detail
            p.info == null
        where:
            providedStatus << [
                409,
                410,
                500
            ]
    }
}
