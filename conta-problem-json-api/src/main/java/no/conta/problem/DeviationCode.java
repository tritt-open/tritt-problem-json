/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

public interface DeviationCode extends CodeAware {

    /**
     * Application level deviation code
     *
     * @return The code of the deviation
     */
    int getCode();

    /**
     * Detail describing the deviation
     *
     * @return The detail that describes the deviation
     */
    default String getDetail() {
        return null;
    }

    /**
     * The default problem level of the deviation code,
     * override to return an actual value.
     *
     * @return level defined for the deviation
     */
    default ProblemLevel getLevel() {
        return null;
    }

    /**
     * Adds the deviation code to the deviation code registry,
     * used to avoid duplicate codes.
     */
    static void add(DeviationCode deviationCode) {
        CodeRegistry.add(deviationCode);
    }

    /**
     * Map from code to deviation code, performs lookup in the deviation code registry.
     */
    static <DC extends DeviationCode> DC fromValue(int value, Class<DC> type) {
        return CodeRegistry.mapCodeFromValue(value, type);
    }

}
