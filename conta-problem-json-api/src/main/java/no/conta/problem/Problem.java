/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Problem information defined to match the application/problem+json (RFC 7807) specification.
 *
 * <p>
 * In addition to the standard fields type, title, status, detail and instance, there are defined:
 * <ol>
 *  <li>code - application level error code</li>
 *  <li>level - info, warning, error or fatal</li>
 *  <li>info - which includes extra information about the actual problem</li>
 *  <li>trace - tracing information</li>
 * </ol>
 * </p>
 *
 * @see ThrowableProblem
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Introspected
public class Problem {

    public static final URI DEFAULT_TYPE = URI.create("about:blank");

    @Nullable
    @Builder.Default
    private URI type = DEFAULT_TYPE;

    @NonNull private String title;
    @NonNull private Integer status;
    @Nullable private String detail;
    @Nullable private URI instance;

    @Nullable private String systemName;
    @Nullable private Integer code;
    @Nullable private ProblemLevel level;
    @Nullable private Map<String, Object> info;

    @Nullable private Trace trace;

    /**
     * Creates a new instance of {@link Problem}.
     *
     * @param status The HTTP status of the response
     * @param title  The description of the HTTP status
     * @return The new instance of problem
     */
    public static Problem of(int status, @NonNull String title) {
        return of(status, title, null);
    }

    /**
     * Creates a new instance of {@link Problem}.
     *
     * @param status The HTTP status of the response
     * @param title  The description of the HTTP status
     * @param detail The detail information about the problem
     * @return The new instance of problem
     */
    public static Problem of(int status, @NonNull String title, String detail) {
        return Problem.builder()
            .title(title)
            .status(status)
            .detail(detail)
            .build();
    }

    /**
     * Creates a new instance of {@link Problem}.
     *
     * @param status The http status code
     * @param detail The detail describing the problem
     * @param code   The code of the problem
     * @param info   The additional information about the problem
     * @return The new instance of {@link ThrowableProblem}
     */
    public static Problem of(Integer status, String detail, Integer code, Map<String, Object> info) {
        return Problem.builder()
            .status(status)
            .detail(detail)
            .code(code)
            .info(info)
            .build();
    }

    /**
     * Sets the info map.
     */
    public Problem withInfo(Map<String, Object> map) {

        setInfo(map);

        return this;
    }

    /**
     * Convenience method used to build info map allowing null values.
     */
    public Problem withInfo(Object... keyValues) {
        return withInfo(Info.of(keyValues));
    }

    /**
     * Adds information about the problem in the info map
     *
     * @param key   The key of the new entry
     * @param value The value of the new entry
     * @return The instance of the problem
     */
    public Problem with(String key, Object value) {
        if (info == null) {
            info = new HashMap<>();
        }
        info.put(key, value);
        return this;
    }

    /**
     * Sets the trace information for the problem
     *
     * @param trace The trace information
     * @return The instance of the problem
     */
    public Problem withTrace(Trace trace) {
        setTrace(trace);
        return this;
    }

    /**
     * Sets the level for the problem
     *
     * @param level The problem level
     * @return The instance of the problem
     */
    public Problem withLevel(ProblemLevel level) {
        setLevel(level);
        return this;
    }

    /**
     * Creates an instance of {@link Problem} using the status defined in the problem code.
     *
     * @param problemCode The problem code
     * @return The new instance of {@link ThrowableProblem}
     */
    public static Problem of(ProblemCode problemCode) {
        return Problem.builder()
            .status(problemCode.getStatus())
            .detail(problemCode.getDetail())
            .code(problemCode.getCode())
            .build();
    }

    /**
     * Creates an instance of {@link Problem}.
     *
     * @param status      The HTTP status code
     * @param problemCode The problem code
     * @return The new instance of {@link ThrowableProblem}
     */
    public static Problem of(Integer status, ProblemCode problemCode) {
        return Problem.builder()
            .status(status != null ? status : problemCode.getStatus())
            .detail(problemCode.getDetail())
            .code(problemCode.getCode())
            .build();
    }

    /**
     * Instantiates a {@link ThrowableProblem} for the problem.
     *
     * @return the {@link ThrowableProblem} instance
     */
    public ThrowableProblem toThrowable() {
        return new ThrowableProblem(
            getStatus(), getDetail(), getCode(), getSystemName(), getInfo()
        );
    }

    /**
     * Instantiates a {@link ThrowableProblem} for the problem
     *
     * @param message Message of the {@link ThrowableProblem}
     * @return the {@link ThrowableProblem} instance
     */
    public ThrowableProblem toThrowable(String message) {
        return new ThrowableProblem(
            getStatus(), getDetail(), getCode(), getSystemName(), getInfo(), message
        );
    }

    /**
     * Instantiates a {@link ThrowableProblem} for the problem
     *
     * @param message Message of the {@link ThrowableProblem}
     * @param cause   Cause of the {@link ThrowableProblem}
     * @return the {@link ThrowableProblem} instance
     */
    public ThrowableProblem toThrowable(String message, Throwable cause) {
        return new ThrowableProblem(
            getStatus(), getDetail(), getCode(), getSystemName(), getInfo(), message, cause
        );
    }

    /**
     * Creates a new instance of {@link Problem} from a {@link ThrowableProblem}.
     *
     * @param tp the throwable problem
     * @return the {@link Problem} instance
     */
    public static Problem fromThrowable(ThrowableProblem tp) {
        return Problem.builder()
            .status(tp.getStatus())
            .detail(tp.getDetail())
            .code(tp.getCode())
            .info(tp.getInfo())
            .build();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor(staticName = "of")
    public static class Trace {

        @NonNull private String traceId;
        @NonNull private String spanId;
    }
}
