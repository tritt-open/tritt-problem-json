/*
 * Copyright (c) 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import lombok.Data;

import java.util.Collections;
import java.util.Map;

@Data
@Introspected
public class Deviation {

    @NonNull private final String field;
    @Nullable private final ProblemLevel level;
    @Nullable private final Integer code;
    @Nullable private String messageKey;

    @Nullable private Map<String, Object> info;

    public static Deviation of(String field, ProblemLevel level, Integer code) {
        return new Deviation(field, level, code);
    }

    public static Deviation of(String field, ProblemLevel level, DeviationCode code) {
        return new Deviation(field, level, code.getCode());
    }

    public static Deviation of(String field, DeviationCode code) {
        return new Deviation(
            field,
            code.getLevel(),
            code.getCode()
        );
    }

    public Deviation withMessageKey(String messageKey) {
        setMessageKey(messageKey);
        return this;
    }

    public Deviation withInfo(Map<String, Object> info) {
        setInfo(info);
        return this;
    }

    @Nullable
    public Map<String, Object> getInfo() {
        return info == null ?
            Collections.emptyMap() :
            Collections.unmodifiableMap(info);
    }
}
