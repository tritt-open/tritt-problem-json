package no.conta.problem;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Objects;

/**
 * Inspired by <a href="https://docs.asciidoctor.org/asciidoc/latest/blocks/admonitions/">AsciiDoc Admonitions</a>.
 */
@Introspected
public enum RemarkType {
    Ok,
    Note,
    Tip,
    Important,
    Caution, // act carefully
    Warning, // inform of consequences
    Error;

    public static final Collection<RemarkType> OK_TYPES = EnumSet.of(Ok, Note, Tip, Important);
    public static final Collection<RemarkType> ERROR_TYPES = EnumSet.of(Caution, Warning, Error);

    public static RemarkType fromLevel(ProblemLevel level) {
        if (level == null) {
            return null;
        }
        return switch (level) {
            case Info -> Note;
            case Warning -> Warning;
            case Error, Fatal -> Error;
        };
    }

    @Nullable
    public static RemarkType mostSevereFrom(@Nullable Collection<Remark> remarks) {
        if (remarks == null || remarks.isEmpty()) {
            return null;
        }
        return remarks.stream()
            .map(r -> r.getType())
            .filter(Objects::nonNull)
            .max(Comparator.comparingInt(t -> t.ordinal()))
            .orElse(null);
    }

    /**
     * Only applies to error types and important.
     */
    public RemarkType increase() {
        return switch (this) {
            case Important -> Caution;
            case Caution -> Warning;
            case Warning -> Error;
            default -> this;
        };
    }

    /**
     * Only applies to error types and important.
     */
    public RemarkType decrease() {
        return switch (this) {
            case Error -> Warning;
            case Warning -> Caution;
            case Caution -> Important;
            default -> this;
        };
    }
}
