/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

public interface RemarkCode extends CodeAware {

    /**
     * Application level problem code
     *
     * @return The code of the problem
     */
    int getCode();

    /**
     * Detail describing the problem
     *
     * @return The detail that describes the problem
     */
    default String getDetail() {
        return null;
    }

    /**
     * The default remark type of the remark code,
     * override to return an actual value.
     *
     * @return type defined for the remark
     */
    default RemarkType getType() {
        return null;
    }

    /**
     * Adds the problem code to the problem code registry,
     * used to avoid duplicate codes.
     */
    static void add(RemarkCode remarkCode) {
        CodeRegistry.add(remarkCode);
    }

    /**
     * Map from code to problem code, performs lookup in the problem code registry.
     */
    static <PC extends RemarkCode> PC fromValue(int value, Class<PC> type) {
        return CodeRegistry.mapCodeFromValue(value, type);
    }
}
