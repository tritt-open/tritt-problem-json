/*
 * Copyright (c) 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Package scoped registry of all defined problem codes, exposed by {@link ProblemCode}.
 *
 * <p>Usage for problems and deviations:</p>>
 * <pre><code>
 *     static {
 *         Arrays.stream(values()).forEach(ProblemCode::add);
 *     }
 * </code></pre>
 *
 * <p>Usage for remarks:</p>>
 * <pre><code>
 *     static {
 *         Arrays.stream(values()).forEach(RemarkCode::add);
 *     }
 * </code></pre>
 */
class CodeRegistry {

    private static final Logger log = LoggerFactory.getLogger(CodeRegistry.class);

    private static final Map<Integer, CodeAware> codes = new ConcurrentHashMap<>();
    private static final List<String> duplicatedCodes = new ArrayList<>();

    static void add(CodeAware code) {
        CodeAware previous = codes.putIfAbsent(code.getCode(), code);
        if (previous != null) {
            String msg = "Problem code [" + code + "] duplicates previous code [" + previous.getCode() + ", " + previous + "]";
            log.warn(msg);
            duplicatedCodes.add(msg);
        }
    }

    static <C extends CodeAware> C mapCodeFromValue(int value, Class<C> type) {
        return type.cast(codes.get(value));
    }

    static List<String> getDuplicatedCodes() {
        return Collections.unmodifiableList(duplicatedCodes);
    }
}
