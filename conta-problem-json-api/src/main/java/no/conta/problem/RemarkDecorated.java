/*
 * Copyright (c) 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import io.micronaut.core.annotation.Creator;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Introspected
public class RemarkDecorated<T> implements RemarkAware {

    @NonNull private final T instance;
    @Nullable private final List<Remark> remarks;

    public static <T> RemarkDecorated<T> of(T instance) {
        return new RemarkDecorated<>(instance, null);
    }

    @Creator
    public static <T> RemarkDecorated<T> of(T instance, List<Remark> remarks) {
        return new RemarkDecorated<>(instance, remarks);
    }

    public List<Remark> getRemarks() {
        return remarks == null ?
            Collections.emptyList() :
            Collections.unmodifiableList(remarks);
    }
}
