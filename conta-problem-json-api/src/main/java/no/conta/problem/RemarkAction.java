package no.conta.problem;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import lombok.Data;

import java.util.Map;

@Data
@Introspected
public class RemarkAction {

    @NonNull private final String key;
    @Nullable private Map<String, Object> info;

    public static RemarkAction of(String key) {
        return new RemarkAction(key);
    }

    public static RemarkAction of(String key, Map<String, Object> info) {
        return new RemarkAction(key).withInfo(info);
    }

    public RemarkAction withInfo(Map<String, Object> info) {
        setInfo(info);
        return this;
    }

    /**
     * Convenience method used to build info map allowing null values.
     */
    public RemarkAction withInfo(Object... keyValues) {
        return withInfo(Info.of(keyValues));
    }

}
