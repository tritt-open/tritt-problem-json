package no.conta.problem;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Info {

    private Info() {
    }

    /**
     * Convenience method used to build info map allowing null values.
     */
    public static Map<String, Object> of(Object... keyValues) {
        return arrayToMap(keyValues, true);
    }

    /**
     * Convenience method used to build info map filtering out null values.
     */
    public static Map<String, Object> ofNonNull(Object... keyValues) {
        return arrayToMap(keyValues, false);
    }

    private static Map<String, Object> arrayToMap(Object[] keyValues, boolean includeNullValues) {
        if (keyValues.length % 2 == 1) {
            throw new IllegalArgumentException("The size of key=value pairs must be even");
        }

        Map<String, Object> map = new LinkedHashMap<>(keyValues.length / 2);
        for (int i = 0; i < keyValues.length; i += 2) {

            String key = keyValues[i].toString();
            Object value = keyValues[i + 1];

            if (value == null && !includeNullValues) {
                continue;
            }
            if (map.containsKey(key)) {
                throw new IllegalArgumentException("Duplicate key [" + key + "] provided");
            }

            map.put(key, value);
        }
        return map;
    }

}
