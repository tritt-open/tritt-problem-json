/*
 * Copyright (c) 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Data
@Introspected
public class Remark {

    @NonNull private final String field;
    @Nullable private final RemarkType type;
    @Nullable private final Integer code;
    @Nullable private String messageKey;

    @Nullable private Map<String, Object> info;
    @Nullable private List<RemarkAction> actions;

    public static Remark of(String field, RemarkType type, Integer code) {
        return new Remark(field, type, code);
    }

    public static Remark of(String field, RemarkType type, ProblemCode code) {
        return new Remark(field, type, code.getCode());
    }

    public static Remark of(String field, RemarkType type, RemarkCode code) {
        return new Remark(field, type, code.getCode());
    }

    public static Remark of(String field, RemarkCode code) {
        return new Remark(
            field,
            code.getType(),
            code.getCode()
        );
    }

    public Remark withChangedType(@Nullable RemarkType changedType) {
        var copy = new Remark(
            field,
            changedType,
            code
        );
        copy.setMessageKey(messageKey);
        copy.setInfo(info);
        copy.setActions(actions);
        return copy;
    }

    public Remark scopedTo(@NonNull String parent) {
        var copy = new Remark(
            parent + "." + field,
            type,
            code
        );
        copy.setMessageKey(messageKey);
        copy.setInfo(info);
        copy.setActions(actions);
        return copy;
    }

    /**
     * Create a remark from a deviation. Proceed with caution.
     */
    public static Remark from(Deviation deviation) {
        return new Remark(
            deviation.getField(),
            RemarkType.fromLevel(deviation.getLevel()),
            deviation.getCode()
        )
            .withMessageKey(deviation.getMessageKey())
            .withInfo(deviation.getInfo());
    }

    public Remark withMessageKey(String messageKey) {
        setMessageKey(messageKey);
        return this;
    }

    public Remark withInfo(Map<String, Object> info) {
        setInfo(info);
        return this;
    }

    /**
     * Convenience method used to build info map allowing null values.
     */
    public Remark withInfo(Object... keyValues) {
        return withInfo(Info.of(keyValues));
    }

    public Remark withAction(RemarkAction action) {
        return withActions(List.of(action));
    }

    public Remark withActions(@NonNull Collection<RemarkAction> actionsToAdd) {
        if (actions == null) {
            actions = new ArrayList<>(actionsToAdd);
        } else {
            actions.addAll(actionsToAdd);
        }
        return this;
    }

    public Map<String, Object> getInfo() {
        return info == null ?
            Collections.emptyMap() :
            Collections.unmodifiableMap(info);
    }

    public boolean is(@NonNull RemarkCode rc) {
        return Objects.equals(getCode(), rc.getCode());
    }

    public boolean is(@NonNull RemarkType rt) {
        return Objects.equals(getType(), rt);
    }
}
