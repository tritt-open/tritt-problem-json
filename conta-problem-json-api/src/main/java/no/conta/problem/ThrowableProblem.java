/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Collections;
import java.util.Map;

/**
 * Definition of a throwable problem that will be translated to application/problem+json.
 *
 * @see Problem
 * @see RuntimeException
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ThrowableProblem extends RuntimeException {

    private final Integer status;
    private final String detail;
    private final Integer code;
    private final String systemName;
    private final Map<String, Object> info;

    /**
     * Creates a new instance of {@link ThrowableProblem}.
     *
     * @param status     The status of the problem
     * @param detail     The detail of the problem
     * @param code       The code that identifies the problem
     * @param systemName The name of the system where the problem has occurred
     * @param info       Additional information about the problem
     */
    public ThrowableProblem(
        Integer status,
        String detail,
        Integer code,
        String systemName,
        Map<String, Object> info
    ) {
        this(status, detail, code, systemName, info, null, null);
    }

    /**
     * Creates a new instance of {@link ThrowableProblem}.
     *
     * @param status     The status of the problem
     * @param detail     The detail of the problem
     * @param code       The code that identifies the problem
     * @param systemName The name of the system where the problem has occurred
     * @param info       Additional information about the problem
     * @param message    The message of the exception
     */
    public ThrowableProblem(
        Integer status, String detail, Integer code, String systemName, Map<String, Object> info, String message
    ) {
        this(status, detail, code, systemName, info, message, null);
    }

    /**
     * Creates a new instance of {@link ThrowableProblem}.
     *
     * @param status     The status of the problem
     * @param detail     The detail of the problem
     * @param code       The code that identifies the problem
     * @param systemName The name of the system where the problem has occurred
     * @param info       Additional information about the problem
     * @param message    The message of the exception
     * @param cause      The cause of the exception
     */
    public ThrowableProblem(
        Integer status, String detail, Integer code, String systemName, Map<String, Object> info, String message, Throwable cause
    ) {
        super(message, cause);

        this.status = status;
        this.detail = detail;
        this.code = code;
        this.systemName = systemName;
        this.info = info != null ? Collections.unmodifiableMap(info) : null;
    }
}
