/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class StandardProblemCodeConstants {

    static final int BAD_REQUEST_STATUS = 400;
    static final int CONFLICT_STATUS = 409;
    static final int GONE_STATUS = 410;
    static final int INTERNAL_SERVER_ERROR_STATUS = 500;
    static final int NOT_IMPLEMENTED_STATUS = 501;

    static final int UNDEFINED_PROBLEM_CODE = 1000;
    static final String UNDEFINED_PROBLEM_DETAIL = "Undefined problem";

    static final int CONSTRAINT_VIOLATION_CODE = 1001;
    static final String CONSTRAINT_VIOLATION_DETAIL = "Constraint violation";

    static final int GONE_CODE = 1002;
    static final String GONE_DETAIL = "Constraint violation";

    static final int NOT_IMPLEMENTED_CODE = 1003;
    static final String NOT_IMPLEMENTED_DETAIL = "Resource is not implemented yet";

    static final int REMOTE_SERVICE_FAILURE_CODE = 1004;
    static final String REMOTE_SERVICE_FAILURE_DETAIL = "A remote service has failed";

    static final int CONFLICT_CODE = 1005;
    static final String CONFLICT_DETAIL = "Constraint violation";
}
