/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem;

import io.micronaut.core.annotation.NonNull;

import java.util.Arrays;

import static no.conta.problem.StandardProblemCodeConstants.BAD_REQUEST_STATUS;
import static no.conta.problem.StandardProblemCodeConstants.CONFLICT_CODE;
import static no.conta.problem.StandardProblemCodeConstants.CONFLICT_DETAIL;
import static no.conta.problem.StandardProblemCodeConstants.CONFLICT_STATUS;
import static no.conta.problem.StandardProblemCodeConstants.CONSTRAINT_VIOLATION_CODE;
import static no.conta.problem.StandardProblemCodeConstants.CONSTRAINT_VIOLATION_DETAIL;
import static no.conta.problem.StandardProblemCodeConstants.GONE_CODE;
import static no.conta.problem.StandardProblemCodeConstants.GONE_DETAIL;
import static no.conta.problem.StandardProblemCodeConstants.GONE_STATUS;
import static no.conta.problem.StandardProblemCodeConstants.INTERNAL_SERVER_ERROR_STATUS;
import static no.conta.problem.StandardProblemCodeConstants.NOT_IMPLEMENTED_CODE;
import static no.conta.problem.StandardProblemCodeConstants.NOT_IMPLEMENTED_DETAIL;
import static no.conta.problem.StandardProblemCodeConstants.NOT_IMPLEMENTED_STATUS;
import static no.conta.problem.StandardProblemCodeConstants.REMOTE_SERVICE_FAILURE_CODE;
import static no.conta.problem.StandardProblemCodeConstants.REMOTE_SERVICE_FAILURE_DETAIL;
import static no.conta.problem.StandardProblemCodeConstants.UNDEFINED_PROBLEM_CODE;
import static no.conta.problem.StandardProblemCodeConstants.UNDEFINED_PROBLEM_DETAIL;

public enum StandardProblemCode implements ProblemCode {

    UndefinedProblem(UNDEFINED_PROBLEM_CODE, BAD_REQUEST_STATUS, UNDEFINED_PROBLEM_DETAIL),
    ConstraintViolation(CONSTRAINT_VIOLATION_CODE, BAD_REQUEST_STATUS, CONSTRAINT_VIOLATION_DETAIL),
    Conflict(CONFLICT_CODE, CONFLICT_STATUS, CONFLICT_DETAIL),
    Gone(GONE_CODE, GONE_STATUS, GONE_DETAIL),
    NotImplemented(NOT_IMPLEMENTED_CODE, NOT_IMPLEMENTED_STATUS, NOT_IMPLEMENTED_DETAIL),
    RemoteServiceFailure(REMOTE_SERVICE_FAILURE_CODE, INTERNAL_SERVER_ERROR_STATUS, REMOTE_SERVICE_FAILURE_DETAIL);

    static {
        Arrays.stream(values()).forEach(ProblemCode::add);
    }

    private final int code;
    private final int status;
    private final String detail;

    StandardProblemCode(int code, int status, String detail) {
        this.code = code;
        this.status = status;
        this.detail = detail;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    @NonNull
    public Integer getStatus() {
        return status;
    }

    @Override
    @NonNull
    public String getDetail() {
        return detail;
    }
}
