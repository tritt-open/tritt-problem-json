/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut.example;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;

import jakarta.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import no.conta.problem.Problem;

// tag::class-def[]
@Controller("/pets")
public class PetController {

    Map<String, Pet> petRepository = new ConcurrentHashMap<>();

    // end::class-def[]
    @Post
    public Pet addPet(@Valid @Body Pet pet) {

        petRepository.put(pet.getName(), pet);
        return pet;
    }

    // tag::compose-error[]
    @Get("/{name}")
    public Pet findPetByName(@PathVariable String name) {
        return petRepository.computeIfAbsent(
            name,
            (missing) -> {
                throw Problem.of(HttpStatus.NOT_FOUND.getCode(), PetProblemCode.PetNotFound)
                    .withInfo("name", missing)
                    .toThrowable();
            }
        );
    }
    // end::compose-error[]

    @Get
    List<Pet> findAllPets() {
        return List.copyOf(petRepository.values());
    }
}
