/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// tag::source[]
package no.conta.problem.micronaut.example;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.HttpStatus;

import no.conta.problem.ProblemCode;

public enum PetProblemCode implements ProblemCode {

    PetNotFound(2000, HttpStatus.NOT_FOUND, "Not possible to find pet");

    private final int code;
    private final Integer status;
    private final String detail;

    PetProblemCode(int code, HttpStatus status, String detail) {
        this.code = code;
        this.status = status.getCode();
        this.detail = detail;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Nullable
    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public String getDetail() {
        return detail;
    }
}
// end::source[]
