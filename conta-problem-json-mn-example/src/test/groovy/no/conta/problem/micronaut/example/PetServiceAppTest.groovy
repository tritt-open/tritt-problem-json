/*
 * Copyright (c) 2021-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut.example

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Specification

import jakarta.inject.Inject
import java.time.LocalDate

import no.conta.problem.Problem
import no.conta.problem.StandardProblemCode

@MicronautTest
class PetServiceAppTest extends Specification {

    @Inject
    @Client('/')
    HttpClient client

    def 'The app works as expected'() {
        given:
            def pet = new Pet(name: 'ContaRabbit', birthDate: LocalDate.of(2021, 10, 26))
        expect:
            client.toBlocking().retrieve(HttpRequest.GET('/pets'), Argument.listOf(Pet)).empty
        when:
            def created = client.toBlocking().exchange(HttpRequest.POST('/pets', pet), Pet).getBody()
        then:
            !created.empty
            created.get() == pet
        and:
            client.toBlocking().retrieve(HttpRequest.GET('/pets'), Argument.listOf(Pet)).size() == 1
        when:
            def retrieved = client.toBlocking().retrieve(HttpRequest.GET('/pets/ContaRabbit'), Pet)
        then:
            retrieved == pet
    }

    def 'Code defined throwable problems are handled correctly'() {
        when: 'requesting a pet that does not exist'
            client.toBlocking().retrieve(HttpRequest.GET('/pets/contaCat'), Argument.of(Pet))
        then: 'the throwable problem is correctly map to the expected http status and problem'
            def error = thrown(HttpClientResponseException)
            error.status == HttpStatus.NOT_FOUND
            def problem = error.response.getBody(Problem).get()
            problem.status == HttpStatus.NOT_FOUND.code
            problem.title == HttpStatus.NOT_FOUND.reason
            problem.code == PetProblemCode.PetNotFound.code
            problem.detail == PetProblemCode.PetNotFound.detail
            problem.info.size() == 1
            problem.info.name == 'contaCat'
    }

    def 'Validation errors are automatically handled correctly'() {
        when: 'submitting a pet with constraint violations'
            client.toBlocking().exchange(HttpRequest.POST('/pets', new Pet()), Pet)
        then:
            def error = thrown(HttpClientResponseException)
            error.status == HttpStatus.BAD_REQUEST
            def problem = error.response.getBody(Problem).get()
            problem.status == HttpStatus.BAD_REQUEST.code
            problem.title == HttpStatus.BAD_REQUEST.reason
            problem.code == StandardProblemCode.ConstraintViolation.code
            problem.detail == StandardProblemCode.ConstraintViolation.detail
            problem.info.size() == 1
            def violations = problem.info.violations as List<Map<String, String>>
            violations.size() == 2
            violations.containsAll(
                [
                    [field: 'addPet.pet.name', message: 'must not be empty'],
                    [field: 'addPet.pet.birthDate', message: 'must not be null']
                ]
            )
    }
}
