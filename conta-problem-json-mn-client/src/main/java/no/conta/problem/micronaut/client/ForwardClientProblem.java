/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut.client;

import io.micronaut.aop.Around;
import io.micronaut.http.client.exceptions.HttpClientResponseException;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import no.conta.problem.Problem;
import no.conta.problem.ThrowableProblem;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * When a class or method if annotated with {@link ForwardClientProblem}, it will be automatically handle the {@link HttpClientResponseException}
 * that contains a {@link Problem} instance as body so a {@link ThrowableProblem} is thrown instead of the original client exception.
 *
 * This is handy when a service wants to forward the same problem that was thrown in another service.
 *
 * @see ForwardClientProblemInterceptor
 */
@Documented
@Retention(RUNTIME)
@Target({TYPE, METHOD})
@Inherited
@Around
public @interface ForwardClientProblem {
}
