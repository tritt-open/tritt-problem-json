/*
 * Copyright (c) 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.conta.problem.micronaut.client;

import io.micronaut.aop.InterceptorBean;
import io.micronaut.aop.MethodInterceptor;
import io.micronaut.aop.MethodInvocationContext;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Singleton;

import no.conta.problem.Problem;
import no.conta.problem.ThrowableProblem;

/**
 * It intercepts methods that are directly annotated with {@link ForwardClientProblem} or belong to a class annotated with that annotation.
 *
 * It catches {@link HttpClientResponseException} and extracts the information from the {@link Problem} instance.
 * It creates a new {@link ThrowableProblem} instance based on the data from the {@link Problem}.
 *
 * It no problem is present in the response it throws the original {@link HttpClientResponseException}.
 */
@Singleton
@InterceptorBean(ForwardClientProblem.class)
public class ForwardClientProblemInterceptor implements MethodInterceptor<Object, Object> {

    private static final Logger log = LoggerFactory.getLogger(ForwardClientProblemInterceptor.class);

    @Override
    public Object intercept(MethodInvocationContext<Object, Object> context) {

        try {
            return context.proceed();
        } catch (HttpClientResponseException exception) {

            log.error(exception.getMessage());
            throw exception.getResponse().getBody(Problem.class)
                .map(problem -> (RuntimeException) problem.toThrowable(exception.getMessage(), exception))
                .orElse(exception);
        }
    }
}
